package com.awe;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import android.content.ContentValues;
import android.database.sqlite.SQLiteConstraintException;
import android.util.Log;

public class FeedParser {
    // We don't use namespaces
    private static final String ns = null;
    
    public void save( String url ) {
    	List items = null;
		try {
			items = load( url );
			for (int i = 0; i < items.size(); i++) {
				ContentValues values = (ContentValues) items.get(i);
				try {
					AWEActivity.db.insertOrThrow(DBHelper.TABLE, null, values);
				} catch ( SQLiteConstraintException e ) {
					//TODO
					Log.d("awe", "not unique guid");
				}
			}
		} catch (IOException e) {
			//TODO
		    Log.d("awe", "connection error");
		    //getResources().getString(R.string.connection_error);
		} catch (JDOMException e) {
			//TODO
		    Log.d("awe", "xml error");
		    //getResources().getString(R.string.xml_error);
		}
    	if ( items != null ){
    		//TODO
    	}
    }
    
	public List load( String url ) throws JDOMException, IOException {
		
		InputStream stream = null;
		List<ContentValues> items;

		try {
			stream = download( url );
			items = parse( stream );
			Log.d("awe", String.format("%s", items.size()));
		} finally {
	        if (stream != null) {
	            try {
					stream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        } 
	     }
		
		return items;
	}
    
	public InputStream download(String urlString) throws IOException {
	    URL url = new URL(urlString);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setReadTimeout(10000 /* milliseconds */);
	    conn.setConnectTimeout(15000 /* milliseconds */);
	    conn.setRequestMethod("GET");
	    conn.setDoInput(true);
	    // Starts the query
	    conn.connect();
	    return conn.getInputStream();
	}
   
    public List<ContentValues> parse(InputStream in) throws JDOMException, IOException {
  	  	List list = new ArrayList();
  	  	List<ContentValues> items = new ArrayList();

		try{
  	  		SAXBuilder builder = new SAXBuilder();
  	    	Document document = (Document) builder.build(in);
  			Element rootNode = document.getRootElement();
  			Element channel = rootNode.getChild("channel");
  			if ( channel != null ) {
  	  			list = channel.getChildren("item");
  			}
  			if ( list == null ) {
  				list = rootNode.getChildren("entry");
  			}
  			if ( list != null ) {
  				for (int i = 0; i < list.size(); i++) {	 
  					Element node = (Element) list.get(i);
  					String title = node.getChildText("title");
  					String guid = node.getChildText("guid");
  					if ( guid == null ){
  						guid = node.getChildText("id");
  					}
  					String permalink = node.getChildText("link");
  					if ( permalink == null ) {
  						Element link = node.getChild("link");
  						permalink = link.getAttributeValue("href");
  					}
  					
  					String body = node.getChildText("description");
  					if ( body == null ) {
  						body = node.getChildText("content");
  					}
  					
  					String _date = node.getChildText("pubDate");
					SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
  					if ( _date == null ) {
  						_date = node.getChildText("published");
  						formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz");
  					}  					

					Date date = null;

  					try {
						date = formatter.parse(_date);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					
					if ( date != null ) {
						formatter = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss",Locale.ENGLISH);
						formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
						ContentValues values = new ContentValues();
						values.put(DBHelper.C_GUID, guid);
						values.put(DBHelper.C_TITLE, title);
						values.put(DBHelper.C_DATE, formatter.format(date));
						values.put(DBHelper.C_PERMALINK, permalink);
						values.put(DBHelper.C_BODY, body);
						items.add(values);
						//db.update(DBHelper.TABLE, values, whereClause, whereArgs)
						//Log.d("AWE", String.format("date:%s", formatter.format(date)));
					} else {
						Log.d("AWE", "NO DATE");
					}
  				}
  			}
  	  	} catch (NullPointerException e) {
  	  	}
		return items;
    }
}