package com.awe;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.json.simple.JSONValue;
import org.json.simple.JSONObject;

import android.app.Activity;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

public class AWEActivity extends Activity {
	
	public static JSONObject SOURCES;
	static public final int NTHREDS = 20;
	
	static public DBHelper dbhelper;
	static public SQLiteDatabase db;
       
    //private static final Context Context = null;
	HTML5WebView mWebView;
	
	public void getFeeds(){
	    Iterator sources = AWEActivity.SOURCES.entrySet().iterator();
	    List urls = new ArrayList();
	    
	    ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
	    	    
	    while(sources.hasNext()){
	        Map.Entry entry = (Map.Entry)sources.next();
		    JSONObject value = (JSONObject)entry.getValue();	
		    String url = value.get("url").toString();
		    Runnable worker = new FeedRunnable( url );
		    executor.execute(worker);  
	    }	
	    
	    // Accept no more workers
	    executor.shutdown();
	    
	    // Wait until all threads are finish
	    //while (!executor.isTerminated()) {
	    //}
	    
	    // We are done
	    //Log.d("awe", "Finished all threads");	    
	}
	
	public void getSources() {
		InputStream raw = getResources().openRawResource(R.raw.sources);
	    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

	    int i;
	    try {
	        i = raw.read();
	        while (i != -1) {
	            byteArrayOutputStream.write(i);
	            i = raw.read();
	        }
	        raw.close();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    
	    Object parsed = JSONValue.parse(byteArrayOutputStream.toString());	
	    SOURCES=(JSONObject)parsed;

	}
       
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWebView = new HTML5WebView(this);
        
    	dbhelper = new DBHelper(this);
    	db = dbhelper.getWritableDatabase();
    	
        mWebView.addJavascriptInterface(new JSInterface(), "awe");
        
        //JSInterface js = new JSInterface();
        //Log.d("awe", js.toString());
       
        if (savedInstanceState != null) {
                mWebView.restoreState(savedInstanceState);
        } else {
                mWebView.loadUrl("file:///android_asset/www/index.html");
        }
       
        setContentView(mWebView.getLayout());
        
        this.getSources();
        this.getFeeds();
    }
   
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
    }
   
    @Override
    public void onStop() {
        super.onStop();
        mWebView.stopLoading();
    }
   
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mWebView.inCustomView()) {
                mWebView.hideCustomView();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
   
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
         super.onConfigurationChanged(newConfig);
    }
}