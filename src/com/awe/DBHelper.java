package com.awe;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
	
	private static final String TAG = "awe";
	public static final String DB_NAME = "posts.db";
	public static final int DB_VERSION = 3;
	public static final String TABLE = "posts";
	//public static final String C_ID = BaseColumns._ID; // Special for ID
	public static final String C_GUID = "guid";
	public static final String C_TITLE = "title";
	public static final String C_DATE = "date";
	public static final String C_PERMALINK = "permalink";
	public static final String C_BODY = "body";
		
	public DBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = String.format("CREATE TABLE %s ( %s TEXT PRIMARY KEY, %s TEXT, %s TEXT, %s TEXT, %s TEXT )", 
				TABLE, C_GUID, C_TITLE, C_DATE, C_PERMALINK, C_BODY );
		
//		sql = context.getString(R.string.sql)
		Log.d(TAG, "onCreate sql:" + sql);
		db.execSQL(sql);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE);
		this.onCreate(db);
	}
}
